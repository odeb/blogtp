<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PostModel;
use App\Models\LuModel;

class PostController extends BaseController {
    public function index() {
        $postModel = new PostModel();
        $data['titre'] = 'Liste des Posts';
        $data['posts'] = $postModel->findAll();
        return view('Post-index.php', $data);
    }

    public function lu($id, $idUser) {
        $luModel = new LuModel();
        $data['idPost'] = $id;
        $data['idUser'] = $idUser;
        $luModel->insert($data);
    }

    public function nonLus() {
        // à coder
    }
}
