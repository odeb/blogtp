<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class BlogSeeder extends Seeder
{
    public function run() {
        for ($i = 0; $i < 10; $i++) {
            $this->db->table('posts')->insert($this->createTask());
        }
    }

    private function createTask(): array {
        $faker = Factory::create();
        return [
            'post' => $faker->sentence(20,true),
            'uri'=>$faker->imageurl(),
            'published'=>0
        ];
    }
}
