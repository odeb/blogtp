<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class BlogMigration extends Migration {
    public function up() {
        // posts
        $this->forge->addField([
            'id'        => ['type' => 'BIGINT',  'constraint' => 11, 'unsigned' => true, 'auto_increment' => true,],
            'post'      => ['type' => 'VARCHAR', 'constraint' => '256', 'null' => false,],
            'uri'       => ['type' => 'VARCHAR', 'constraint' => '256', 'null' => false,],
            'image'     => ['type' => 'VARCHAR', 'constraint' => '256', 'null' => false,],
            'published' => ['type' => 'TINYINT', 'constraint' => 1],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('posts');
        // comments
        $this->forge->addField([
            'id'       => ['type' => 'BIGINT', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
            'text'     => ['type' => 'VARCHAR', 'constraint' => '256', 'null' => false],
            'idUser'   => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('idUser', 'users', 'id', '', 'CASCADE');
        $this->forge->createTable('comments');
        // lus
        $this->forge->addField([
            'idUser'   => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
            'idPost'   => ['type' => 'BIGINT', 'constraint' => 11, 'unsigned' => true],
        ]);
        $this->forge->addKey(['idUser', 'idPost']);
        $this->forge->addForeignKey('idUser', 'users', 'id', '', 'CASCADE');
        $this->forge->addForeignKey('idPost', 'posts', 'id', '', 'CASCADE');
        $this->forge->createTable('lus', true);
    }

    public function down() {
        $this->forge->dropTable('lus');
        $this->forge->dropTable('comments');
        $this->forge->dropTable('posts');
    }
}
