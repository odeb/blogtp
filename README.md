# Blog

## 1. Cas d'utilisation

```plantuml
left to right direction

actor :user:
actor :blogueur:
actor :visiteur:
actor :commentateur:


package blog {
    user--(authentification)
    visiteur -u- (readPost)
    blogueur -d- (createPost)
    blogueur -d- (readPost)
    blogueur -d- (updatePost)
    blogueur -d- (deletePost)  

    commentateur -- (addComment)
    commentateur -- (readPost)
    
    blogueur--|>user
    commentateur--|>user

}
```

## Création des tables

Création des tables utilisateurs

```sh
php spark migrate -n "Myth\Auth"
```

Création des tables blog

```sh
php spark migrate -n "App"
```
